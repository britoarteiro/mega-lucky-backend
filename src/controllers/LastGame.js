
const Games = require('../models/Games')

module.exports = {
  async index(req, res){
    const game = await Games.findOne({}, {}, { sort: {createdAt: "desc" } });
    return res.json(game)
  }
}