const axios = require('axios')
const Games = require('../models/Games')

module.exports = {
	async index(req, res){
		const games = await Games.find({}).sort({createdAt: "desc" }).catch((err => { console.log(err)}))
		return res.json(games)
	},
  async store(req, res) {
    
    const response = await axios.get('https://apiloterias.com.br/app/resultado?loteria=megasena&token=0TQU6iadwyWG0Qz')

		const game = await Games.create({game: response.data})
		
		return res.json(game)
  }
}