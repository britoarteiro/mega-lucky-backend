
const Games = require('../models/Games')

module.exports = {
  async index(req, res){
    const games = await Games.find({}).sort({createdAt: "desc" }).limit(req.body.limitNumbers);

    let allDozens = []
    let numArr = []
    games.map( game => {
      game.game.dezenas.map(num => {
        numArr.push(parseInt(num,10))
      })
      allDozens = [...numArr]
    })

    const findDuplicatedInArray = (arr) => {
      const count = {}
      const result = []

      arr.forEach(item => {
        if (count[item]) {
          count[item] +=1
          return
        }
          count[item] = 1
      })
    
      for (let prop in count){
        if (count[prop] >=2){
            result.push(prop)
        }
      }

      return count;
    }

    const buildFullNumbers = (obj) => {
      const objEntries = Object.entries(obj)
      const drawnedKeys = Object.keys(obj)
      const buildedObject = []

      for (let i = 1; i <= 60; i++) {
        i = i.toString()
        let drawnedCountNumber

        if (drawnedKeys.includes(i)) {
          objEntries.forEach((objEntry) => {
            if (objEntry[0] === i) {
              drawnedCountNumber = objEntry[1]
            }
          })
        } else {
          drawnedCountNumber = 0
        }

        const buildedObj = { label: i, drawnedCount: drawnedCountNumber }

        buildedObject.push(JSON.parse(JSON.stringify(buildedObj)))
      }

      return buildedObject;
    }

    const drawnedNumbers = findDuplicatedInArray(allDozens)

    return res.json(buildFullNumbers(drawnedNumbers))
  }
}