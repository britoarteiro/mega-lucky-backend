const express = require('express');

const routes = express.Router();

const GamesController = require('./controllers/GamesController');
const LastGame = require('./controllers/LastGame');
const DrawnedNumbersController = require('./controllers/DrawnedNumbersController');
const YourLuckGame = require('./controllers/YourLuckGame')

routes.get('/games', GamesController.index)
routes.post('/games', GamesController.store)

routes.get('/last-game', LastGame.index)
routes.post('/drawned-numbers', DrawnedNumbersController.index)
routes.get('/your-luck-game', YourLuckGame.index)

module.exports = routes;