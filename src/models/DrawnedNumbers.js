const { Schema, model} = require('mongoose');

const DrawnedNumbersSchema = new Schema({
  drawnedNumbers: Object
}, {
	timestamps: true
});

module.exports = model('DrawnedNumbers', DrawnedNumbersSchema)