const { Schema, model} = require('mongoose');

const GamesSchema = new Schema({
  game: Object
}, {
	timestamps: true
});

module.exports = model('Games', GamesSchema)